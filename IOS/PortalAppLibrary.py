class PortalAppLibrary(object):
    """
    This is a document for Appium control Android and iOS
    """
    ### Appium Server ###
    def init_appium_server(self, _Port, _bootstarpPort, _phoneUUID):
        """Initialize Appium server with Port, BootStrapPort and PhoneUUID """
        raise Exception(NotImplemented)
    def start_appium_server(self):
    	"""Start Appium Server which has been initialized"""
        raise Exception(NotImplemented)
    def stop_appium_server(self):
    	"""Stop Appium Server when the testing is Done"""
        raise Exception(NotImplemented)
    def init_appium_setting(self, _phoneOS, _phoneOSVersion, _Port):
    	"""Setting Appium Server for test device which installed Portal APP"""
        raise Exception(NotImplemented)
    def launch_app(self):
    	"""Run app on testing device via appium"""
    	raise Exception(NotImplemented)
    def close_app(self):
    	"""Stop app on testing device via appium"""
    	raise Exception(NotImplemented)
    ### Fisrt Page ###
    def buy_portal(self):
        """Buy button on "Start up" page"""
        raise Exception(NotImplemented)
    def setup_portal(self):
    	"""Setup button on "Start up" page"""
        raise Exception(NotImplemented)
    ### On-board Portal ###
    def help_onboard(self):
        raise Exception(NotImplemented)
    def next_onboard(self,count = 1):
        raise Exception(NotImplemented)
    def skip_onboard(self):
        raise Exception(NotImplemented)
    def done_onboard(self):
        raise Exception(NotImplemented)
    def cancel_locating(self):
        raise Exception(NotImplemented)
    def found_portal(self, _portalID):
        raise Exception(NotImplemented)
    def pair_portal(self):
        raise Exception(NotImplemented)
    def name_customize(self, _ssid):
        raise Exception(NotImplemented)
    def password_customize(self, _password):
        raise Exception(NotImplemented)
    def skip_customize(self):
        raise Exception(NotImplemented)
    def apply_customize(self):
        """Click apply button at the Setup your network page."""
        raise Exception(NotImplemented)
    def home_onboard(self):
        """Click home button at the "All done!" page."""
        raise Exception(NotImplemented)
    ### Portal App Home page ###
    def internet(self):
        raise Exception(NotImplemented)
    def portal_setting(self):
        raise Exception(NotImplemented)
    def guest(self):
        raise Exception(NotImplemented)
    def device(self):
        raise Exception(NotImplemented)
    def add_portal(self):
        raise Exception(NotImplemented)
    def network_setting(self):
    	"""Click "Gear Mark" for newrok setting at the Portal Home page"""
        raise Exception(NotImplemented)
    def help(self):
    	"""Click "Question Mark" for help at the Portal Home page"""
        raise Exception(NotImplemented)
    def mynetwork(self):
        raise Exception(NotImplemented)
    ### Internet Settings Page ###
    def show_wan_ip(self):
        raise Exception(NotImplemented)
    ### Portal Settings Page ###
    def get_ssid(self):
        raise Exception(NotImplemented)
    def change_ssid(self, ssid):
        raise Exception(NotImplemented)
    def get_password(self):
        raise Exception(NotImplemented)
    def change_password(self, _password):
        raise Exception(NotImplemented)
    def enable_wireless_security_mode(self):
        raise Exception(NotImplemented)
    def show_advanced_device_settings(self):
        raise Exception(NotImplemented)
    def seperate_network(self):
        raise Exception(NotImplemented)
    def show_operating_channel(self):
        raise Exception(NotImplemented)
    def webgui_enable(self):
        raise Exception(NotImplemented)
    def bridge_mode(self):
        raise Exception(NotImplemented)
    def compatibility_mode(self):
        raise Exception(NotImplemented)
    def apply_setting(self):
    	raise Exception(NotImplemented)
    def wait_setting_change_done(self):
    	raise Exception(NotImplemented)
    ### Compatibility Mode ###
    def compatibility_mode_enable(self):
        raise Exception(NotImplemented)
    def mode_a(self):
        raise Exception(NotImplemented)
    def mode_b(self):
        raise Exception(NotImplemented)
    def mode_c(self):
        raise Exception(NotImplemented)
    def apply_compatibility_mode(self):
        raise Exception(NotImplemented)
    def back_compatibility_mode(self):
        raise Exception(NotImplemented)
    ### Guest Page ###
    def back_user_and_guest(self):
        raise Exception(NotImplemented)
    def add_guest_network(self):
        raise Exception(NotImplemented)
    def set_ssid_guest_network(self, _ssid):
        raise Exception(NotImplemented)
    def set_password_guest_network(self, _password):
        raise Exception(NotImplemented)
    def save_guest_network(self):
        raise Exception(NotImplemented)
    def back_guest_network(self):
        raise Exception(NotImplemented)
    ### Device Page ###
    def back_client_devices(self):
        raise Exception(NotImplemented)
    def show_client_devices(self):
        raise Exception(NotImplemented)
    def reflash_client_devices(self):
        raise Exception(NotImplemented)
    ### network setting Page ###
    def back_network_setting(self):
        raise Exception(NotImplemented)
    def check_update(self):
        raise Exception(NotImplemented)
    def accept_information(self):
        raise Exception(NotImplemented)
    def restart_network(self):
        raise Exception(NotImplemented)
    def accept_restart_network(self):
        raise Exception(NotImplemented)
    def cancel_restart_network(self):
        raise Exception(NotImplemented)
    def remove_as_admin(self):
        raise Exception(NotImplemented)
    def accept_remove_as_admin(self):
        raise Exception(NotImplemented)
    def cancel_remove_as_admin(self):
        raise Exception(NotImplemented)
    ### Help Page ###
    def back_help(self):
        raise Exception(NotImplemented)
    def led_color_chart(self):
        raise Exception(NotImplemented)
    def help_center(self):
        raise Exception(NotImplemented)
    def chat_with_us(self):
        raise Exception(NotImplemented)
    def my_support_tickets(self):
        raise Exception(NotImplemented)
    def rate_our_app(self):
        raise Exception(NotImplemented)
	