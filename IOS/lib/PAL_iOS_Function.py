from PAL_iOS_Base import PAL_iOS_Base


class PAL_iOS_Function(PAL_iOS_Base):
    ROBOT_LIBRARY_SCOPE = 'TEST_SUITE'
    def delete_network(self):
    	#close app
    	self.close_app()
    	#launch app
    	self.launch_app()
    	if self.check_portal_onboard() == True:
    		#Delete network
    		self.network_setting()
    		self.remove_as_admin()
    		self.accept_remove_as_admin()
    	self.check_in_main_page()

    def launch_app_goto_main_page(self,_phoneOS, _phoneOSVersion, _phoneUUID, _Port, _bootstarpPort):
    	self.start_appium_and_launch_app(_phoneOS, _phoneOSVersion, _phoneUUID, _Port, _bootstarpPort)
    	if self.check_portal_onboard() == True:
    		self.delete_network()
    		self.close_app()
    	else:
    		#close app 
    		self.close_app()
    	#launch app
    	self.launch_app()
    	#check buy button or check setup button exist
    	print self.check_in_main_page()

    def help_page(self):

    	#TODO
    	pass

    def instruction_page(self):
    	#TODO
    	pass

    def find_portal(self, portalId):
        print('Find Portal %s', portalId)
        self.setup_portal()
        self.next_onboard(3)
        self.done_onboard()
        self.found_portal(portalId)
        pass

        
