import time
from PAL_iOS_Function import PAL_iOS_Function
from selenium.webdriver.common.keys import Keys

class PAL_iOS_TestCase(PAL_iOS_Function):
    ROBOT_LIBRARY_SCOPE = 'TEST_SUITE'

    def quick_start_guide(self):
        self.setup_portal()
        self.help()
        self.click_table_cell(0, 0)
        self.next_onboard(3)
        self.done_onboard()
        self.check_test_result("Quick start guide", 'quick_start_guide')

    def instruction_page(self):
        self.setup_portal()
        self.next_onboard(3)
        self.check_test_result("Done", 'instruction_page')

    def skip_to_find_portal(self, portalId):
        print('Find Portal %s', portalId)
        self.setup_portal()
        skip_btn = self.find_element_by_name('Skip', 1)
        skip_btn.click()
        self.click_next_allow_locationg()
        self.found_portal(portalId)
        print('[test result] skip_to_find_portal success')

    def click_next_allow_locationg(self):
        print("Click next button at the Locating-your-portal page.")
        try:
            self.click_button("Next")
        except:
            return

    def cancel_find_portal(self):
        self.setup_portal()
        skip_btn = self.find_element_by_name('Skip', 1)
        skip_btn.click()
        cancel_btn = self.find_element_by_name('Cancel', 1)
        cancel_btn.click()
        self.check_test_result("Buy", 'cancel_find_portal')

    def onboard_portal(self, portalId):
        self.skip_to_find_portal(portalId)
        self.pair_portal()
        self.click_customize()
        self.check_test_result("Setup your network", 'onboard_portal')

    def skip_to_pair_portal(self, portalId):
        self.skip_to_find_portal(portalId)
        self.pair_portal()
        self.skip_settings()

    def skip_settings(self):
        self.click_customize()
        self.skip_customize()

    def customize_to_pair_portal(self, portalId, ssid, password):
        self.skip_to_find_portal(portalId)
        self.pair_portal()
        self.customize_settings(ssid, password)

    def customize_settings(self, ssid, password):
        self.click_customize()
        self.customize_portal(ssid, password)

    def customize_portal(self, ssid, password):
        self.name_customize(ssid)
        self.password_customize(password)
        self.apply_customize()
        time.sleep(120)
        next_btn = self.find_element_by_name('Next', 120)
        next_btn.click()
        self.click_home()
        self.check_test_result("Internet", 'customize_portal')

    def remove_admin(self):
        self.network_setting()
        self.remove_as_admin()
        self.accept_remove_as_admin()
        self.check_test_result("Buy", 'remove_admin', 120)

    def restart_network(self):
        self.network_setting()
        self.click_table_cell(1, 1)
        restart_btn = self.find_element_by_name('RESTART', 2)
        restart_btn.click()

    def enable_web_gui(self):
        self.portal_setting()
        switch = self.find_elements_by_id("WebGUI Enabled")
        switch[1].click()
        self.apply_setting()

    def separate_network(self):
        self.portal_setting()
        switch = self.find_elements_by_id("Separate Networks (2.4G/5G)")
        switch[1].click()
        self.apply_setting()

    def enable_beamforming(self):
        self.portal_setting()
        self.driver.swipe(start_x=60, start_y=300, end_x=60, end_y=-200, duration=500)
        switch = self.find_elements_by_id("Beamforming", 5)
        switch[1].click()
        self.apply_setting()

    def select_compatibility_mode(self, mode):
        self.portal_setting()
        self.driver.swipe(start_x=60, start_y=300, end_x=60, end_y=-300, duration=500)
        cell = self.find_elements_by_id("Compatibility Mode", 5)
        cell[0].click()
        enable_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeSwitch", 120)
        enable_btn.click()
        self.click_table_cell(1, mode)
        self.apply_setting()

    def add_mesh_only_one(self):
        self.find_mesh_portal()
        done_btn = self.find_element_by_name('Home', 240)
        done_btn.click()
        time.sleep(10)
        self.check_test_result_via_item_count("network_portal", 'add_mesh', 2)

    def find_mesh_portal(self):
        add_btn = self.find_element_by_name('add portal', 90)
        add_btn.click()
        continue_btn = self.find_element_by_name('Continue', 90)
        continue_btn.click()
        next_btn = self.find_element_by_name('Next', 90)
        next_btn.click()
        done_btn = self.find_element_by_name('Done', 90)
        done_btn.click()

    def set_bridge_in_ap_settings(self):
        self.portal_setting()
        self.driver.swipe(start_x=60, start_y=300, end_x=60, end_y=-200, duration=500)
        self.click_table_cell(1, 8)
        time.sleep(30)
        bridge_switch = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeSwitch", 2)
        bridge_switch.click()
        self.apply_setting()

    def set_bridge_onboard(self, portalId):
        self.skip_to_find_portal(portalId)
        self.pair_portal()
        time.sleep(30)
        mode_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton", 2)
        mode_btn.click()
        cell_bridge = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[3]", 2)
        cell_bridge.click()
        bridge_switch = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeSwitch", 2)
        bridge_switch.click()
        apply_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeButton[2]", 2)
        apply_btn.click()
        self.click_customize()
        self.check_test_result("Setup your network", 'set_bridge_onboard')

    def set_pppoe_onboard(self, portalId, account, password, service):
        self.skip_to_find_portal(portalId)
        self.pair_portal()
        time.sleep(30)
        mode_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton", 2)
        mode_btn.click()
        cell_pppoe = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]", 2)
        cell_pppoe.click()
        pppoe_account = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField")
        pppoe_account.click()
        pppoe_account.send_keys(account)
        pppoe_account.send_keys(Keys.RETURN)
        pppoe_password = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeSecureTextField", 5)
        pppoe_password.click()
        pppoe_password.send_keys(password)
        pppoe_password.send_keys(Keys.RETURN)
        pppoe_service = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeTextField", 5)
        pppoe_service.click()
        pppoe_service.send_keys(service)
        pppoe_service.send_keys(Keys.RETURN)
        apply_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeButton[2]", 10)
        apply_btn.click()
        self.click_customize()
        self.check_test_result("Setup your network", 'set_pppoe_onboard')

    def set_static_ip_onboard(self, portalId, ipAddress, netmask, gateway, dns):
        self.skip_to_find_portal(portalId)
        self.pair_portal()
        time.sleep(30)
        mode_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton", 2)
        mode_btn.click()
        cell_static = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[4]", 2)
        cell_static.click()
        static_ip = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTextField")
        static_ip.click()
        static_ip.clear()
        static_ip.send_keys(ipAddress)
        static_ip.send_keys(Keys.RETURN)
        static_netmask = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeTextField", 5)
        static_netmask.click()
        static_netmask.clear()
        static_netmask.send_keys(netmask)
        static_netmask.send_keys(Keys.RETURN)
        static_gateway = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeTextField", 5)
        static_gateway.click()
        static_gateway.clear()
        static_gateway.send_keys(gateway)
        static_gateway.send_keys(Keys.RETURN)
        static_dns = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeTextField", 5)
        static_dns.click()
        static_dns.clear()
        static_dns.send_keys(dns)
        static_dns.send_keys(Keys.RETURN)
        apply_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther/XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeOther[5]/XCUIElementTypeButton[2]", 10)
        apply_btn.click()
        self.click_customize()
        self.check_test_result("Setup your network", 'set_static_ip_onboard')

    def set_ssid(self, ssid):
        self.portal_setting()
        ssid_field = self.find_elements_by_type("XCUIElementTypeTextField", 90)
        ssid_field[0].click()
        ssid_field[0].clear()
        ssid_field[0].send_keys(ssid)
        ssid_field[0].send_keys(Keys.RETURN)
        self.apply_setting()
        self.check_set_ssid(ssid)

    def check_set_ssid(self, ssid):
        time.sleep(120)
        self.portal_setting()
        self.check_test_result(ssid, 'set_ssid')

    def set_password(self, password):
        self.portal_setting()
        password_field = self.find_elements_by_type("XCUIElementTypeSecureTextField", 90)
        password_field[0].click()
        password_field[0].clear()
        password_field[0].send_keys(password)
        password_field[0].send_keys(Keys.RETURN)
        self.apply_setting()
        self.check_set_password(password)

    def check_set_password(self, password):
        time.sleep(120)
        self.portal_setting()
        password_show = self.find_element_by_name("show password", 90)
        password_show.click()
        self.check_test_result(password, 'set_password')

    def add_guest(self, ssid, password):
        self.portal_guest()
        add_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeOther[3]/XCUIElementTypeButton", 90)
        add_btn.click()
        ssid_field = self.find_elements_by_type("XCUIElementTypeTextField", 90)
        ssid_field[0].click()
        ssid_field[0].clear()
        ssid_field[0].send_keys(ssid)
        ssid_field[0].send_keys(Keys.RETURN)
        password_field = self.find_elements_by_type("XCUIElementTypeSecureTextField", 90)
        password_field[0].click()
        password_field[0].clear()
        password_field[0].send_keys(password)
        password_field[0].send_keys(Keys.RETURN)
        save_btn = self.find_element_by_name('Save', 90)
        save_btn.click()
        self.check_test_result(ssid, 'add_guest', 120)

    def delete_guest(self):
        self.portal_guest()
        self.driver.swipe(start_x=200, start_y=380, end_x=-200, end_y=0, duration=1000)
        time.sleep(1)
        print('here33')

    def add_muti_portal_skip_customize(self, portalId):
        self.onboard_muti_portal(portalId)
        self.skip_settings()

    def add_muti_portal_via_customize(self, portalId, ssid, password):
        self.onboard_muti_portal(portalId)
        self.customize_settings(ssid, password)
        self.check_add_muti_portal_via_customize(ssid)

    def check_add_muti_portal_via_customize(self, ssid):
        time.sleep(120)
        self.click_menu()
        self.check_test_result_via_item_count(ssid, 'add_muti_portal_via_customize')

    def onboard_muti_portal(self, portalId):
        time.sleep(10)
        self.click_menu()
        add_btn = self.find_element_by_name("+")
        add_btn.click()
        self.found_portal(portalId)
        self.pair_portal()

    def client_devices(self):
        self.portal_devices()
        refresh_btn = self.find_element_by_name("notifications refresh")
        refresh_btn.click()
        self.check_test_result("Client Devices", 'client_devices')

    def onboard_quick_start_guide(self):
        self.portal_help()
        self.click_table_cell(0, 0)
        self.check_test_result("Here's what you need", 'onboard_quick_start_guide')

    def onboard_led_color(self):
        self.portal_help()
        self.click_table_cell(0, 1)
        self.check_test_result("Portal status lights", 'onboard_led_color')

    def onboard_help_center(self):
        self.portal_help()
        self.click_table_cell(0, 2)
        self.check_test_result("Help", 'onboard_help_center')

    def onboard_chart(self):
        self.portal_help()
        self.click_table_cell(0, 3)
        self.check_test_result("Chat", 'onboard_chart')

    def onboard_support_tickets(self):
        self.portal_help()
        self.click_table_cell(0, 4)
        self.check_test_result("My tickets", 'onboard_support_tickets')

    def onboard_rate(self):
        self.portal_help()
        self.click_table_cell(0, 5)
        self.check_test_result("portalwifi.com", 'onboard_rate')

    def disable_security_mode(self):
        self.portal_setting()
        self.driver.swipe(start_x=100, start_y=300, end_x=0, end_y=-300, duration=500)
        time.sleep(2)
        switch = self.find_elements_by_id("Turn off wireless security")
        switch[1].click()
        # self.driver.swipe(start_x=100, start_y=100, end_x=0, end_y=300, duration=500)
        # time.sleep(2)
        # segment_5g = self.find_element_by_name("5G")
        # segment_5g.click()
        # self.driver.swipe(start_x=100, start_y=300, end_x=0, end_y=-300, duration=500)
        # time.sleep(2)
        # switch = self.find_elements_by_id("Turn off wireless security")
        # switch[1].click()
        self.apply_setting()

    def check_upgrade(self):
        self.network_setting()
        self.click_table_cell(1, 0)

    def add_mesh(self, portalId):
        self.find_mesh_portal()
        self.found_portal(portalId)
        self.configure_portal()
        done_btn = self.find_element_by_name('Home', 240)
        done_btn.click()
        time.sleep(10)
        self.check_test_result_via_item_count("network_portal", 'add_mesh', 2)




