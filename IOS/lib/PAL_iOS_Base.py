import os
import sys
import random
import time
import urllib2
import unittest
import appium
from robot.api import logger
from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.keys import Keys
from AppiumControl import AppiumControl

parentdir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(parentdir + "/..")
from PortalAppLibrary import PortalAppLibrary

class FailedActionError(RuntimeError):
    def __init__(self, arg):
        self.args = arg

class PAL_iOS_Base(PortalAppLibrary):
    ROBOT_LIBRARY_SCOPE = 'TEST_SUITE'
    def init_appium_server(self, _Port, _bootstarpPort, _phoneUUID):
        self.AppiumControl = AppiumControl(int(_Port), int(_bootstarpPort), _phoneUUID)
        
    def start_appium_server(self):
        self.AppiumControl.start()
        
    def stop_appium_server(self):
    	self.AppiumControl.stop()

    def init_appium_setting(self, _phoneOS, _phoneOSVersion, _phoneUUID, _Port, device_name = "MyPhone"):
        curDir = os.path.dirname(os.path.abspath(__file__))
        desired_caps = {}
        desired_caps['newCommandTimeout'] = '1200'
        desired_caps['platformName'] = _phoneOS
        desired_caps['platformVersion'] = _phoneOSVersion
        desired_caps['deviceName'] = device_name
        ### autoGrantPremissions only for Android ###
        #desired_caps['autoGrantPermissions'] = True
        desired_caps['fullReset'] = False
        desired_caps['noReset'] = True
        desired_caps['bundleId'] = 'com.portalwifi.portal'
        desired_caps['automationName'] = 'XCUITest'
        #desired_caps['xcodeConfigFile'] = curDir + '/myconfig.xcconfig'
        # desired_caps['realDeviceLogger'] = '/usr/local/lib/node_modules/deviceconsole'
        desired_caps['udid'] = _phoneUUID
        self.driver = webdriver.Remote(('http://127.0.0.1:%d/wd/hub' % int(_Port)), desired_caps)

    def start_appium_and_launch_app(self, _phoneOS, _phoneOSVersion, _phoneUUID, _Port, _bootstarpPort):
        self.init_appium_server(_Port, _bootstarpPort, _phoneUUID)
        self.start_appium_server()
        self.init_appium_setting(_phoneOS, _phoneOSVersion, _phoneUUID, _Port)

    def launch_app(self):
    	self.driver.launch_app()

    def close_app(self):
    	self.driver.close_app()

    def check_test_result(self, itemName, resultMsg, timeout=20):
        if self.wait_for_name(itemName):
            print('[TestResult] %s success' %resultMsg)
        else:
            print('[TestResult] %s failed' %resultMsg)

    # def check_test_result_via_item_not_exist(self, itemName, resultMsg, timeout=20):
    #     if self.wait_for_name(itemName):
    #         itme = self.find_element_by_name(itemName)
    #         print('[TestResult] %s failed' %resultMsg)
    #         print(itme)
    #     else:
    #         print('[TestResult] %s success' %resultMsg)

    def check_test_result_via_item_count(self, itemName, resultMsg, targetCount, timeout=20):
        itemList = self.find_elements_by_id(itemName)
        if len(itemList) == targetCount:
            print('[TestResult] %s success' %resultMsg)
        else:
            print('[TestResult] %s failed' %resultMsg)

    ### Fisrt Page ###
    def buy_portal(self):
        buy_btn = self.find_element_by_name('Buy', 90)
        buy_btn.click()
        self.check_test_result("Portal Wifi", 'buy_portal')

    def setup_portal(self):
    	setup_btn = self.find_element_by_name('Set up', 90)
        setup_btn.click()
        self.check_test_result("Here's what you need", 'setup_portal')

    ### On-board Portal ###
    #def help_onboard(self):
    def next_onboard(self,count = 1):
        for i in range(0, count, 1):
            next_btn = self.find_element_by_name('Next', 90)
            next_btn.click()    

    #def skip_onboard(self):
    def done_onboard(self):
        done_btn = self.find_element_by_name('Done', 90)
        done_btn.click()

    #def cancel_locating(self):
    # def found_portal(self, _portalID):
    #     portal = self.find_element_by_name('Portal ' + _portalID, 90)
    #     portal.click()

    def found_portal(self, name, maxswipes=10):
        print("select a portal on portal list page.")

        count = 0
        page_last_portal = []
        prev_search_last_portal = ""
        #print prev_search_last_portal

        while count <= maxswipes:
            count += 1
            if self.wait_for_id("Customize"):
                return

            portal_list = self.driver.find_elements_by_xpath('''//XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText''')
            print "portal list count: ", len(portal_list)

            if len(portal_list) == 0:
                if count < maxswipes:
                    time.sleep(5)
                    print "sleep 5 seconds and try again"
                    continue
                else:
                    raise FailedActionError("Portal %s not found." %name)                

            if prev_search_last_portal == portal_list[-1]:
                raise FailedActionError("Portal %s not found." %name)

            prev_search_last_portal = portal_list[-1]
            for item in portal_list:
                print "one portal: ", item.text
                if name in item.text:
                    item.click()
                    return item

            self.driver.swipe(start_x=360, start_y=900, end_x=360, end_y=300, duration=2000)
            if count == maxswipes:
                raise FailedActionError("Portal ID: %s not found in list. Exiting." %name)


    def pair_portal(self):
        pair_btn = self.find_element_by_name('Pair')
        pair_btn.click()

    def click_customize(self):
        customize_btn = self.find_element_by_name('Customize', 120)
        customize_btn.click()

    def configure_portal(self):
        configure_btn = self.find_element_by_name('Configure')
        configure_btn.click()

    def click_menu(self):
        menu = self.find_element_by_name("ic menu")
        menu.click()

    def name_customize(self, _ssid):
        ssid_field = self.find_elements_by_type("XCUIElementTypeTextField", 90)
        ssid_field[0].click()
        ssid_field[0].clear()
        ssid_field[0].send_keys(_ssid)
        ssid_field[0].send_keys(Keys.RETURN)
        logger.info("Customize SSID as %s Done" %(_ssid))

    def password_customize(self, _password):
        password_field = self.find_elements_by_type("XCUIElementTypeSecureTextField", 90)
        password_field[0].click()
        password_field[0].clear()
        password_field[0].send_keys(_password)
        password_field[0].send_keys(Keys.RETURN)
        logger.info("Customize Password Done")

    def hide_keyboard(self):
        #done_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow[4]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeKeyboard/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton[4]")
        done_btn = self.find_element_by_name('Done', 180)
        done_btn.click()

    def skip_customize(self):
        skip_btn = self.find_element_by_name('Skip')
        skip_btn.click()
        time.sleep(15)
        next_btn = self.find_element_by_name('Next', 10)
        next_btn.click()
        self.click_home()
        self.check_test_result("Internet", 'skip_customize')

    def apply_customize(self):
        apply_btn = self.find_element_by_name('Apply')
        apply_btn.click()

    def next_to_home(self):
    	next_btn = self.find_element_by_name('Next')

    def click_home(self):
        home_btn = self.find_element_by_name('Home')
    	home_btn.click()
        time.sleep(10)

    ### Portal App Home page ###
    #def internet(self):
    def portal_setting(self):
        #portal_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeStaticText")
        time.sleep(20)
    	portal_btn = self.find_element_by_name('Portal', 90)
    	portal_btn.click()
        time.sleep(2)
        self.check_test_result("BASIC DEVICE SETTINGS", 'portal_setting')

    def portal_guest(self):
        time.sleep(20)
        guest_btn = self.find_element_by_name('Guests', 90)
        guest_btn.click()
        time.sleep(2)

    def portal_devices(self):
        time.sleep(20)
        devices_btn = self.find_element_by_name('Devices', 90)
        devices_btn.click()
        time.sleep(2)

    def portal_help(self):
        time.sleep(20)
        help_btn = self.find_element_by_name("network help")
        help_btn.click()
        time.sleep(2)

    #def guest(self):
    def device(self):
    	device_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[9]/XCUIElementTypeStaticText[2]")
        device_btn.click()

    def add_portal(self):
        add_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[3]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeButton")
        add_btn.click()

    def network_setting(self):
        time.sleep(20)
    	setting_btn = self.find_element_by_name('network setting', 90)
    	setting_btn.click()
        time.sleep(2)

    def help(self):
    	"""Click "Question Mark" for help at the setup page"""
        help_btn = self.find_element_by_name('Help')
        help_btn.click()

    #def mynetwork(self):
    ### Internet Settings Page ###
    #def show_wan_ip(self):
    ### Portal Settings Page ###
    def portal_setting_back(self):
    	back_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeNavigationBar/XCUIElementTypeButton", 90)
    	#back_btn = self.find_element_by_name('Back', 90)
    	back_btn.click()
    #def get_ssid(self):
    def change_ssid(self, _ssid):
        ssid_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeTextField", 90)
        ssid_btn.click()
        ssid_btn.clear()
        #ssid_btn.send_keys(_ssid)
        ssid_btn.set_value(_ssid)
        logger.info("Customize SSID as %s Done" %(str(_ssid)))
        #done_btn = self.find_element_by_name('Done', 90)
        #done_btn.click()

    #def get_password(self):
    def change_password(self, _password):
    	pwd_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeSecureTextField", 90)
        pwd_btn.click()
        pwd_btn.clear()
        pwd_btn.send_keys(_password)

    #def enable_wireless_security_mode(self):
    #def show_advanced_device_settings(self):
    #def seperate_network(self):
    #def show_operating_channel(self):
    #def webgui_enable(self):
    #def bridge_mode(self):
    #def compatibility_mode(self):
    def apply_setting(self):
    	apply_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeNavigationBar/XCUIElementTypeButton[2]",90)
    	apply_btn.click()

    def wait_setting_change_done(self):
    	back_btn = self.find_element_by_xpath("//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeNavigationBar/XCUIElementTypeButton", 120)
    ### Compatibility Mode ###
    #def compatibility_mode_enable(self):
    #def mode_a(self):
    #def mode_b(self):
    #def mode_c(self):
    #def apply_compatibility_mode(self):
    #def back_compatibility_mode(self):
    ### Guest Page ###
    #def back_user_and_guest(self):
    #def add_guest_network(self):
    #def set_ssid_guest_network(self, _ssid):
    #def set_password_guest_network(self, _password):
    #def save_guest_network(self):
    #def back_guest_network(self):
    ### Device Page ###
    #def back_client_devices(self):
    #def show_client_devices(self):
    #def reflash_client_devices(self):
    ### network setting Page ###
    #def back_network_setting(self):
    #def check_update(self):
    #def accept_information(self):
    #def restart_network(self):
    #def accept_restart_network(self):
    #def cancel_restart_network(self):
    def remove_as_admin(self):
        delete_btn = self.find_element_by_name('REMOVE AS ADMIN')
    	delete_btn.click()

    def accept_remove_as_admin(self):
        remove_btn = self.find_element_by_name('REMOVE AS ADMIN')
    	remove_btn.click()

    #def cancel_remove_as_admin(self):
    ### Help Page ###
    #def back_help(self):
    #def led_color_chart(self):
    #def help_center(self):
    #def chat_with_us(self):
    #def my_support_tickets(self):
    #def rate_our_app(self):
    def check_portal_onboard(self, timeout=10):
        return self.wait_for_name('Portal', timeout)

    def check_in_main_page(self, timeout=10):
        return self.wait_for_name('Buy', timeout)

    ### Tool ###
    def find_element_by_name(self, _name, timeout=10):
        """Custom find element by id with a 10 sec wait for item to show up"""
        self.wait_for_name(_name, timeout)
        return self.driver.find_element_by_name(_name)

    def wait_for_name(self, _name, timeout=20):
        """Wait for id element to show up
        Has a default 10 sec timeout.
        """
        try:
            WebDriverWait(self.driver, timeout).until(
                    EC.presence_of_element_located((By.NAME, _name))
                    )
            return True
        except TimeoutException:
            logger.info("Can not find button")
            return False  

    def find_element_by_xpath(self, _xpath, timeout=10):
        """Custom find element by id with a 10 sec wait for item to show up"""
        self.wait_for_xpath(_xpath, timeout)
        return self.driver.find_element_by_xpath(_xpath)

    def wait_for_xpath(self, _xpath, timeout=20):
        """Wait for id element to show up
        Has a default 10 sec timeout.
        """
        try:
            WebDriverWait(self.driver, timeout).until(
                    EC.presence_of_element_located((By.XPATH, _xpath))
                    )
            return True
        except TimeoutException:
            print ("Can not find button")
            return False

    def find_elements_by_id(self, _id, timeout=10):
        """Custom find element by id with a 10 sec wait for item to show up"""
        self.wait_for_id(_id, timeout)
        return self.driver.find_elements_by_accessibility_id(_id)

    def wait_for_id(self, _id, timeout=20):
        """Wait for id element to show up
        Has a default 10 sec timeout.
        """
        try:
            WebDriverWait(self.driver, timeout).until(
                    EC.presence_of_element_located((By.ID, _id))
                    )
            return True
        except TimeoutException:
            print ("Can not find button")
            return False

    def find_elements_by_type(self, _type, timeout=10):
        """Custom find element by id with a 10 sec wait for item to show up"""
        self.wait_for_type(_type, timeout)
        return self.driver.find_elements_by_class_name(_type)

    def wait_for_type(self, _type, timeout=20):
        """Wait for id element to show up
        Has a default 10 sec timeout.
        """
        try:
            WebDriverWait(self.driver, timeout).until(
                    EC.presence_of_element_located((By.CLASS_NAME, _type))
                    )
            return True
        except TimeoutException:
            print ("Can not find button")
            return False

    def click_table_cell(self, table_index, cell_index):
        table = self.driver.find_elements_by_class_name("XCUIElementTypeTable")[table_index]
        row = table.find_elements_by_class_name("XCUIElementTypeCell")[cell_index]
        row.click()
