import os
import sys
import time
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/lib")
from PAL_iOS_TestCase import PAL_iOS_TestCase

port         = 4724
bsport       = 2987
uuid         = "8932f2bcc06d6a0062350b71c24e7b5136421a29"
phoneOS      = "iOS"
phoneVerison = "9.35"



test = PAL_iOS_TestCase()

# =====Test case list=====

# from onboard
# test.launch_app_goto_main_page(phoneOS, phoneVerison, uuid, port, bsport)
# test.buy_portal()
# test.setup_portal()
# test.quick_start_guide()
# test.instruction_page()
# test.skip_to_find_portal('1B69')
# test.cancel_find_portal()
# test.onboard_portal('1B69')
# test.set_pppoe_onboard('1B69', 'account123', 'password456', 'service789')
# test.set_bridge_onboard('1B69')
# test.set_static_ip_onboard('1B69', '192.168.0.1', '192.168.0.2', '192.168.0.3', '192.168.0.4')
# test.skip_customize()
# test.customize_portal('69', '12344321')

# test.skip_to_pair_portal('1B69')
# test.customize_to_pair_portal('1B69', '69', '12344321')



# from home
# test.start_appium_and_launch_app(phoneOS, phoneVerison, uuid, port, bsport)
# test.onboard_quick_start_guide()
# test.onboard_led_color()
# test.onboard_help_center()
# test.onboard_chart()
# test.onboard_support_tickets()
# test.onboard_rate()
# test.portal_setting()
# test.set_ssid('69zxc')
# test.set_password('qwerrewq')
# test.disable_security_mode()
# test.enable_web_gui()
# test.enable_beamforming()
# test.set_bridge_in_ap_settings()
# test.select_compatibility_mode(1)
# test.client_devices()
# test.add_guest('test01', 'qwerrewq')
# test.delete_guest()
# test.check_upgrade()
# test.restart_network()
# test.remove_admin()
# test.add_mesh_only_one()
# test.add_mesh('1F7D')
# test.add_muti_portal_skip_customize('1F7D')
# test.add_muti_portal_via_customize('1F7D', '7D', '12344321')

# test.separate_network()


test.stop_appium_server()
