import os
import random
import time
import urllib2
import unittest
import appium
from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.keys import Keys

class Android(object):
    def __init__(self, port, unicode_keyboard = None,
            platform_version = None, device_name = None, apk_file = None):

        platform_version = "7.1.1"
        device_name = "Android_Device"

        self.appPackage = 'com.portalwifi.portal.eval'

        self.port = int(port)
        desired_caps = {}
        desired_caps['newCommandTimeout'] = '300'
        desired_caps['platformName'] = "Android"
        desired_caps['platformVersion'] = platform_version
        desired_caps['deviceName'] = device_name
        desired_caps['autoGrantPermissions'] = True
        #desired_caps['app'] = apk
        desired_caps['noReset'] = 'true'
        desired_caps['appPackage'] = self.appPackage
        desired_caps['appActivity'] = 'com.ignitiondl.portal.MainActivity'
        self.driver = webdriver.Remote(('http://127.0.0.1:%d/wd/hub' % self.port), desired_caps)

    def find_by_id(self, item_id, package = None):
        if package == None:
            package = self.appPackage
        element_id = package + ':id/' + item_id
        print 'element id:', element_id
        return self.driver.find_element_by_id(element_id)

    def find_elements_by_id(self, item_id, package = None):
        if package == None:
            package = self.appPackage
        element_id = package + ':id/' + item_id
        print 'element id:', element_id
        return self.driver.find_elements_by_id(element_id)

    def element_exists_by_id(self, my_id, package = None):
        if package == None:
            package = self.appPackage
        element_id = package + ':id/' + my_id
        print 'element id:', element_id
        if len(self.driver.find_elements_by_id(element_id)) == 0:
            return False
        return True

    def click_buy(self):
        print "Click buy button at the portal app start up page."
        buy_btn = self.find_by_id('buy_button')
        buy_btn.click()

    def click_setup(self):
        print "Click setup button at the portal app start up page."
        setup_button = self.find_by_id('setup_button')
        setup_button.click();

    def click_help(self):
        print "Click help button at the manual page"
        help_button = self.find_by_id('button_help')
        help_button.click()

    def click_next(self, count = 1): 
        print "Click next button at the on-bording page."
        for i in range(0, count, 1):
            next_btn = self.find_by_id("button_next")
            next_btn.click()
            time.sleep(1)

    def click_for_allow(self):
        print "Click allow button at the portal ask for device to allow to get location info"
        try:
            self.find_by_id('permission_allow_button', 'com.android.packageinstaller').click()
        except:
           return   

    def click_quick_start(self):
        print "Quick start guide"
        helpList = self.driver.find_elements_by_xpath('//android.widget.ListView/android.widget.LinearLayout');
        helpList[0].click()

    def click_quick_start_next(self):
        #Click quick start guide next button
        nextButton = self.find_by_id('button_next')
        nextButton.click()

    def click_instruction_page_next(self):
        #Click next button at the instruction page
        nextButton = self.find_by_id('button_next')
        nextButton.click()

    def click_skip(self):
        skipButton = self.find_by_id('option_button')
        skipButton.click()

    def scrollToPortal(self, name, maxswipes=10):
        #Scroll until finds name element. Times out after 15 seconds.
        count = 0
        page_last_portal = []
        prev_search_last_portal = ""
        #print prev_search_last_portal
        while True:
            count += 1
            if self.element_exists_by_id("button_apply"):
                return

            portal_list = self.driver.find_elements_by_id("com.portalwifi.portal.eval:id/portal_name")
            print "portal list count: ", len(portal_list)

            if len(portal_list) == 0:
                if count < maxswipes:
                    time.sleep(5)
                    print "sleep 5 seconds and try again"
                    continue
                else:
                    raise FailedActionError("Portal %s not found." %name)  

            # Check if hit end of list
            page_last_portal += portal_list[-1].text
            if prev_search_last_portal == portal_list[-1].text:
                #print "end"
                raise FailedActionError("Portal %s not found." %name)
            prev_search_last_portal = portal_list[-1]
            for elem in portal_list:
                elemText = elem.text.encode("utf-8")
                print(elemText, name)
                if name in elemText:
                    print('exist')
                    #logger.console("Element %s is found" %elem.text)
                    target_portal = elem
                    target_portal.click()
                    return elem
            self.driver.swipe(start_x=360, start_y=900, end_x=360, end_y=300, duration=2000)
            if count == maxswipes:
                #logger.console("Cannot find Portal %s after %s swipes" % (name, maxswipes))
                raise FailedActionError("Portal ID: %s not found in list. Exiting." %name)
            time.sleep(1)

    def click_cancel(self):
        #Click cancel button at the locating page.
        cancel_btn = self.find_by_id('button_1')
        cancel_btn.click()

    def click_pair(self):
        #Click pair button at the locating page.
        try:
           self.find_by_id('button_1').click()
        except:
           return 

    def click_skip(self):
        #Click skip button at the customize page
        skip_btn = self.find_by_id('option_button_layout')
        skip_btn.click()

    def set_name_password(self, ssid, password):
        # Network Name
        #Click name button at the Setup your network page.
        network_name = self.find_by_id("ssid_edit")
        network_name.click()
        network_name.send_keys(ssid)
        # Password
        #Click password button at the Setup your network page.
        network_pwd = self.find_by_id("password_edit")
        network_pwd.click()
        network_pwd.send_keys(password)

    def click_customize(self):
        #Click apply button at the Setup your network page.
        customize_btn = self.find_by_id("button_apply")
        customize_btn.click()

    def click_home(self):
        #Click skip button at the customize page
        home_btn = self.find_by_id('button_home')
        home_btn.click()

    def click_setting(self):
        #Click right-up setting button at the home page.
        setting_btn = self.find_by_id('network_settings')
        setting_btn.click()

    def click_remove_admin(self):
        #Click Remove as Admin button at the portal setting page.
        remove_admin_btn = self.find_by_id("delete_network_button")
        remove_admin_btn.click()

    def click_ok_button(self):
        #Click Remove as Admin button at the portal setting page.
        remove_admin_btn = self.driver.find_element_by_id("android:id/button1")
        remove_admin_btn.click()

    def click_restart_network(self):
        #Click restart network button at the portal setting page.
        restart_network_btn = self.find_by_id("restart_network_button")
        restart_network_btn.click()

    def quick_start_guide(self):
        time.sleep(10)
        self.click_setup()
        time.sleep(1)
        self.click_help()
        time.sleep(1)
        self.click_quick_start()
        time.sleep(1)
        self.click_quick_start_next()
        time.sleep(1)
        self.click_quick_start_next()
        time.sleep(1)
        self.click_quick_start_next()
        time.sleep(1)
        self.click_quick_start_next()

    def instruction_page(self):
        time.sleep(10)
        self.click_setup()
        time.sleep(1)
        self.click_instruction_page_next()
        time.sleep(1)
        self.click_instruction_page_next()
        time.sleep(1)
        self.click_instruction_page_next()

    def find_portal(self, portalId):
        print('        Find Portal %s', portalId)
        time.sleep(10)
        self.instruction_page()
        time.sleep(1)
        self.click_instruction_page_next()
        time.sleep(16)
        self.scrollToPortal(portalId)

    def find_portal_skip(self, portalId):
        print('        Find Portal %s', portalId)
        time.sleep(10)
        self.click_setup()
        time.sleep(1)
        self.click_skip()
        time.sleep(16)
        self.scrollToPortal(portalId)

    def cancel_find_portal(self):
        print('        Cancel Find Portal')
        time.sleep(10)
        self.click_setup()
        time.sleep(1)
        self.click_skip()
        time.sleep(3)
        self.click_cancel()

    def skip_customize(self, portalId):
        self.find_portal_skip(portalId)
        time.sleep(1)
        self.click_pair()
        time.sleep(30)
        self.click_skip()
        time.sleep(30)
        self.click_home()

    def apply_customize(self, portalId, ssid, password):
        self.find_portal_skip(portalId)
        time.sleep(1)
        self.click_pair()
        time.sleep(30)
        self.set_name_password(ssid, password)
        time.sleep(2)
        self.click_customize()
        time.sleep(30)
        self.click_home()

    def delete_admin_network(self):
        time.sleep(10)
        print ("        Delete Network ")
        self.click_setting()
        time.sleep(2)
        self.click_remove_admin()
        time.sleep(3)
        self.click_ok_button()

    def retart_network(self):
        time.sleep(10)
        print ("        Restart Network ")
        self.click_setting()
        time.sleep(2)
        self.click_restart_network()
        time.sleep(3)
        self.click_ok_button()
        time.sleep(3)
        self.click_ok_button()

    def close_appium_session(self):
        time.sleep(10)
        print ("        Session terminated")
        self.driver.quit()

    def onboard_portal(self, potalid, ssid=None, password=None):
        print("        On borading Portal")
        if ssid is None or password is None:
            self.skip_customize(potalid)
        else:
            self.apply_customize(potalid, ssid, password)

    def click_apply_setting(self):
        apply_btn = self.find_by_id("apply_settings")
        apply_btn.click()

    def click_portal_btn(self):
        print("On home page, click portal icon")
        portal_btn = self.find_by_id("portal_button")
        portal_btn.click()

    def click_web_gui(self):
        switch = self.find_by_id("webgui_enabled_switch")
        switch.click()

    def click_separate_network(self):
        switch = self.find_by_id("separate_network_enabled_switch")
        switch.click()

    def click_beamforming(self):
        switch = self.find_by_id("beamforming_switch")
        switch.click()

    def click_compatibility_mode(self):
        view = self.find_by_id("compatibility_mode_view")
        view.click()

    def click_toolbar_back(self):
        back_btn = self.find_by_id("toolbar_back")
        back_btn.click()

    def enable_web_gui(self):
        print("        Enter Portal Device Enable Web GUI")   
        self.click_portal_btn()
        time.sleep(2)
        self.click_web_gui()
        time.sleep(2)
        self.click_apply_setting()
        time.sleep(5)
        self.click_toolbar_back()

    def enable_separate_network(self):
        print("        Enter Portal Device Enable separate network")   
        self.click_portal_btn()
        time.sleep(2)
        self.click_separate_network()
        time.sleep(2)
        self.click_apply_setting()
        time.sleep(60)
        self.click_toolbar_back()

    def enable_beamformin(self):
        print("        Enter Portal Device Enable Web GUI")   
        self.click_portal_btn()
        time.sleep(2)
        self.click_beamforming()
        time.sleep(2)
        self.click_apply_setting()
        time.sleep(5)
        self.click_toolbar_back()

    def select_compatibility_mode(self, mode):
        print("select a mode: 0->A, 1->B, 2->C") 
        if mode > 2 or mode < 0:
            raise FailedActionError("Mode: %d not exist." %name)

        self.click_portal_btn()
        time.sleep(2)
        self.click_compatibility_mode()
        time.sleep(2)
        el = self.find_elements_by_id('mode_item_layout')
        el[mode].click()
        time.sleep(2)
        self.click_apply_setting()
        time.sleep(5)

if __name__ == '__main__':
    apptest = Android("4723")

    #Main page
    time.sleep(10)
    apptest.select_compatibility_mode(0)
    # apptest.onboard_portal('1A41', 'auto test', '1234567890')
    apptest.close_appium_session()
