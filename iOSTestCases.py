
import os
import random
import time
import urllib2
import unittest
import appium
from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.keys import Keys

class iOS(object):
    def __init__(self, port, unicode_keyboard = None,
            platform_version = None, device_name = None, apk_file = None):

        platform_version = "10.2"
        device_name = "Jason\'s iPhone" 
        device_uuid = '0fe1ca8014f00a33e0ee8097565483ff329da0df'
        self.port = int(port)
        desired_caps = {}
        desired_caps['newCommandTimeout'] = '300'
        desired_caps['platformName'] = "iOS"
        desired_caps['platformVersion'] = platform_version
        desired_caps['deviceName'] = device_name
        desired_caps['automationName'] = 'XCUITest'
        desired_caps['noReset'] = 'true'
        desired_caps['bundleId'] = 'com.portalwifi.portal'
        desired_caps['udid'] = device_uuid
        self.driver = webdriver.Remote(('http://127.0.0.1:%d/wd/hub' % self.port), desired_caps)

    def click_button(self, buttonId, buttonIndex=0):
        self.driver.find_elements_by_accessibility_id(buttonId)[buttonIndex].click()

    def click_buy(self):
        print("Click buy button at the portal app start up page.")
        self.click_button("Buy")

    def click_setup(self):
        print("Click setup button at the portal app start up page.")
        self.click_button("Set up")

    def click_skip(self):
        print "Click skip button at the instruction page"
        self.click_button("Skip")

    def click_next(self, count = 1):
        print("Click next button at the on-bording page.")
        for i in range(0, count - 1, 1):
            self.click_button("Next")
            time.sleep(1)
        if count == 4 :
            self.click_button("Done")

    def click_next_to_find_portal(self):
        print("Click next button at the Locating-your-portal page.")
        try:
            self.click_button("Next")
        except:
            return

    def element_exists_by_id(self, my_id):
        if len(self.driver.find_elements_by_accessibility_id(my_id)) == 0:
            return False
        return True

    def scrollToPortal(self, name, maxswipes=10):
        print("select a portal on portal list page.")

        count = 0
        page_last_portal = []
        prev_search_last_portal = ""
        #print prev_search_last_portal

        while True:
            count += 1

            if self.element_exists_by_id("Customize"):
                return

            portal_list = self.driver.find_elements_by_xpath('''//XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeStaticText''')
            print "portal list count: ", len(portal_list)

            if len(portal_list) == 0:
                if count < maxswipes:
                    time.sleep(5)
                    print "sleep 5 seconds and try again"
                    continue
                else:
                    raise FailedActionError("Portal %s not found." %name)                

            if prev_search_last_portal == portal_list[-1]:
                raise FailedActionError("Portal %s not found." %name)

            prev_search_last_portal = portal_list[-1]
            for item in portal_list:
                print "one portal: ", item.text
                if name in item.text:
                    item.click()
                    return item

            self.driver.swipe(start_x=360, start_y=900, end_x=360, end_y=300, duration=2000)
            if count == maxswipes:
                raise FailedActionError("Portal ID: %s not found in list. Exiting." %name)
        time.sleep(1)

    def click_pair(self):
        print "Click setup button at the portal app start up page."
        if self.element_exists_by_id("Pair"):
            self.click_button("Pair")
        else:
            print "No pair button"

    def click_cancel(self):
        print "Click cancel button at the locating page."
        self.click_button("Cancel")

    def click_customize(self):
        print "Click Customize button."
        self.click_button("Customize")

    def click_table_cell(self, table_index, cell_index):
        table = self.driver.find_elements_by_class_name("XCUIElementTypeTable")[table_index]
        row = table.find_elements_by_class_name("XCUIElementTypeCell")[cell_index]
        row.click()

    def finish_pair(self):
        print("next to show network")
        self.click_button("Next")
        print("finish pair to home")
        self.click_button("Home")

    def click_portal_btn(self):
        print("On home page, click portal icon")
        l = self.driver.find_elements_by_accessibility_id('network_background')
        print 'network len: ', len(l)
        if len(l) > 1:
            l[1].click()

    def quick_start_guide(self):
        self.click_setup()
        self.click_button("Help")
        self.click_table_cell(0, 0)
        self.click_next(4)

    def instruction_page(self):
        self.click_setup()
        self.click_next(3)

    def test_customize_skip(self):
        self.click_button("Skip")

    def customize_setting(self, ssid, pwd):
        print("find ssid input")
        ssid_input = self.driver.find_element_by_class_name("XCUIElementTypeTextField")
        print("set ssid")
        ssid_input.send_keys(ssid)
        print("find password input")
        password_input = self.driver.find_element_by_class_name("XCUIElementTypeSecureTextField")
        print("set password")
        password_input.send_keys(pwd)
        time.sleep(1)
        password_input.send_keys(Keys.RETURN)

    def click_apply_for_network_customize(self):
        print "Click apply button at the Setup your network page."
        self.click_button("Apply")

    def check_customize_finish(self, maxswipes=10):
        count = 0
        while count < maxswipes:
            count += 1
            if self.element_exists_by_id('Next'):
                break
            else:
                time.sleep(15)

    def retart_network(self):
        time.sleep(20)
        print("network setting")
        self.click_button("network setting")
        print("click restart")
        self.click_table_cell(1, 1)
        print("conform restart")
        self.click_button("RESTART")

    def delete_admin_network(self):
        time.sleep(20)
        print("network setting")
        self.click_button("network setting")
        print("click remove")
        self.click_button("REMOVE AS ADMIN")
        print("conform remove")
        self.click_button("REMOVE AS ADMIN")

    def cancel_find_portal(self):
        print('        Cancel Find Portal')
        time.sleep(10)
        self.click_setup()
        time.sleep(1)
        self.click_skip()
        time.sleep(3)
        self.click_cancel()

    def find_portal(self, portalId):
        print('        Find Portal %s', portalId)
        time.sleep(10)
        self.click_setup()
        self.click_next(4)
        time.sleep(16)
        self.scrollToPortal(portalId)
        time.sleep(2)

    def find_portal_skip(self, portalId):
        print 'Find Portal %s', portalId
        time.sleep(10)
        self.click_setup()
        time.sleep(1)
        self.click_skip()
        time.sleep(17)
        self.scrollToPortal(portalId)
        time.sleep(2)

    def onboard_portal(self, potalid, ssid=None, password=None):
        print("        On borading Portal")
        self.find_portal_skip(potalid)
        self.click_pair()
        time.sleep(20)
        self.click_customize()
        if ssid is None or password is None:
            self.test_customize_skip()
            time.sleep(1)
        else:
            self.customize_setting(ssid, password)
            time.sleep(10)
            self.click_apply_for_network_customize()
            self.check_customize_finish()

        self.finish_pair()
        time.sleep(20)

    def test_ap_setting(self, ssid, password):
        time.sleep(20)
        print("ap setting")
        # self.click_portal_btn()
        self.click_button("network_background", 1)
        print("find ssid input")
        ssid_input = self.driver.find_element_by_class_name("XCUIElementTypeTextField")
        print("set ssid")
        ssid_input.clear()
        time.sleep(1)
        ssid_input.send_keys(ssid)
        print("find password input")
        password_input = self.driver.find_element_by_class_name("XCUIElementTypeSecureTextField")
        print("set password")
        password_input.send_keys(password)
        time.sleep(1)
        password_input.send_keys(Keys.RETURN)
        time.sleep(1)
        self.click_button("Apply")
        time.sleep(20)

    def close_appium_session(self):
        time.sleep(10)
        print ("        Session terminated")
        self.driver.quit()


    def test_add_mesh(self, portalid=None):
        time.sleep(20)
        print("add mesh")
        self.click_button("add portal")
        self.click_button("Continue")
        self.click_button("Next")
        self.click_button("Done")
        print("find portal for mesh")
        time.sleep(20)
        try:
            choosePortal = self.driver.find_element_by_accessibility_id("Continue")
            print "choose portal: ", portalid
        except:
            print("only one portal, continue set mesh")

        time.sleep(150)
        print("finish mesh")
        self.click_button("Home")

    def click_web_gui(self):
        switch = self.driver.find_elements_by_accessibility_id("WebGUI Enabled")[1]
        switch.click()

    def click_separate_network(self):
        switch = self.driver.find_elements_by_accessibility_id("Separate Networks (2.4G/5G)")[1]
        switch.click()

    def click_beamforming(self):
        print("click_beamforming")   
        self.driver.swipe(start_x=60, start_y=300, end_x=60, end_y=-200, duration=500)
        time.sleep(5)
        switch = self.driver.find_elements_by_accessibility_id("Beamforming")[1]
        switch.click()

    def click_compatibility_mode(self):
        print("click_compatibility_mode")   
        self.driver.swipe(start_x=60, start_y=300, end_x=60, end_y=-300, duration=500)
        time.sleep(5)
        switch = self.driver.find_elements_by_accessibility_id("Compatibility Mode")[0]
        switch.click()

    def enable_web_gui(self):
        print("        Enter Portal Device Enable Web GUI")   
        self.click_portal_btn()
        time.sleep(5)
        self.click_web_gui()  
        time.sleep(2)
        self.click_button("Apply")
        time.sleep(5)

    def enable_separate_network(self):
        print("        Enter Portal Device Enable separate network")   
        self.click_portal_btn()
        time.sleep(2)
        self.click_separate_network()
        time.sleep(2)
        self.click_button("Apply")
        time.sleep(15)

    def enable_beamformin(self):
        print("        Enter Portal Device Enable Web GUI")   
        self.click_portal_btn()
        time.sleep(2)
        self.click_beamforming()
        time.sleep(2)
        self.click_button("Apply")
        time.sleep(5)

    def select_compatibility_mode(self, mode):
        print("select a mode: 0->A, 1->B, 2->C") 
        if mode > 2 or mode < 0:
            raise FailedActionError("Mode: %d not exist." %name)

        self.click_portal_btn()
        time.sleep(2)
        self.click_compatibility_mode()
        time.sleep(20)
        self.click_table_cell(1, mode)
        time.sleep(2)
        self.click_button("Apply")
        time.sleep(5)

if __name__ == '__main__':

    apptest = iOS("4723")
    time.sleep(10)

    # apptest.onboard_portal("1A41", "portal", "1234567890")

    # time.sleep(5)

    apptest.delete_admin_network()

    time.sleep(5)

    apptest.close_appium_session()

