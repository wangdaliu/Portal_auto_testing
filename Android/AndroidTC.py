import os
import sys
import time
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/lib")
from PAL_Android_TestCase import PAL_Android_TestCase

port         = 4724
bsport       = 2987
# uuid         = "FA6BF0302650"
uuid         = "LGLS990971097ef"
phoneOS      = "Android"
# phoneVerison = "7.1.1"
phoneVerison = "6.0.1"

test = PAL_Android_TestCase()

# =====Test case list=====

# from onboard
# test.launch_app_goto_main_page(phoneOS, phoneVerison, uuid, port, bsport)
# test.buy_portal()
# test.setup_portal()
# test.quick_start_guide()
# test.instruction_page()
# test.find_portal('1F7D')
# test.finding_portal_skip('1F7D')
# test.cancel_find_portal()
# test.skip_customize('1A41')
# test.apply_customize('1B69', '69', '12344321')
# test.set_bridge_onboard('0EA9')
# test.set_pppoe_onboard('0EA9', 'account123', 'password456', 'service789')
# test.set_static_ip_onboard('0EA9', '192.168.0.1', '192.168.0.2', '192.168.0.3', '8.8.8.4')

# from home
# test.launch_app_page(phoneOS, phoneVerison, uuid, port, bsport)
# test.delete_network()
# test.restart_network()
# test.enable_web_gui()
# test.enable_separate_network()
# test.enable_beamforming()
# test.select_compatibility_mode(1)
# test.add_mesh('0EA9')
# test.change_router_ssid('41test')
# test.change_router_password('12345678')
# test.enable_turn_off_wireless_security()
# test.create_guest_network('guest1', '12345678')
# test.add_new_portal('0EA9', '0EA9', '12345678')
# test.add_new_portal_skip_customize('0EA9')
# test.refresh_devices()
# test.delete_guest_network()
# test.onboard_quick_start_guide()
# test.onboard_led_color()
# test.onboard_help_center()
# test.onboard_chart()
# test.onboard_support_tickets()
# test.onboard_rate()
# test.set_bridge_in_ap_settings()

test.stop_appium_server()
