import os
import sys
import random
import time
import urllib2
import unittest
import appium
from robot.api import logger
from appium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import WebDriverException

#sys.path.append(os.path.dirname(os.path.abspath(__file__)) + "/lib")
from AppiumControl import AppiumControl

parentdir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(parentdir + "/..")
from PortalAppLibrary import PortalAppLibrary


class FailedActionError(RuntimeError):
    def __init__(self, arg):
        self.args = arg

class PAL_Android_Base(PortalAppLibrary):
    ROBOT_LIBRARY_SCOPE = 'TEST_SUITE'
    def init_appium_server(self, _Port, _bootstarpPort, _phoneUUID):
        self.AppiumControl = AppiumControl(int(_Port), int(_bootstarpPort), _phoneUUID)

    def start_appium_server(self):
        self.AppiumControl.start()

    def stop_appium_server(self):
        self.AppiumControl.stop()

    def init_appium_setting(self, _phoneOS, _phoneOSVersion, _phoneUUID, _Port, device_name = "MyPhone"):
        self.appPackageName = "com.portalwifi.portal.eval"
        desired_caps = {}
        desired_caps['newCommandTimeout'] = '300'
        desired_caps['platformName'] = _phoneOS
        desired_caps['platformVersion'] = _phoneOSVersion
        desired_caps['deviceName'] = device_name
        desired_caps['autoGrantPermissions'] = True
        desired_caps['fullReset'] = False
        desired_caps['noReset'] = True
        desired_caps['appPackage'] = self.appPackageName
        desired_caps['appActivity'] = 'com.ignitiondl.portal.MainActivity'
        self.driver = webdriver.Remote(('http://127.0.0.1:%d/wd/hub' % int(_Port)), desired_caps)

    def start_appium_and_launch_app(self, _phoneOS, _phoneOSVersion, _phoneUUID, _Port, _bootstarpPort):
        self.init_appium_server(_Port, _bootstarpPort, _phoneUUID)
        self.start_appium_server()
        self.init_appium_setting(_phoneOS, _phoneOSVersion, _phoneUUID, _Port)

    def launch_app(self):
        self.driver.launch_app()

    def close_app(self):
        self.driver.close_app()
    ### Fisrt Page ###
    def buy_portal(self):
        buy_btn = self.find_element_by_id("buy_button")
        buy_btn.click()

    def verify_setup(self):
        show_skip_btn = self.wait_for_id('option_button', 5)
        if (show_skip_btn == False):
            raise FailedActionError("Setup failed. Exiting.")

    def click_setup_portal(self):
        setup_btn = self.find_element_by_id("setup_button")
        setup_btn.click()

    ### On-board Portal ###
    #def help_onboard(self):
    def next_onboard(self,count = 1):
        for i in range(0, count, 1):
            next_btn = self.find_element_by_id("button_next",90)
            next_btn.click()

    #def skip_onboard(self):
    def done_onboard(self):
        done_btn = self.find_element_by_id("button_next")
        done_btn.click()

    #def cancel_locating(self):
    def found_portal(self, _portalID):
    	maxswipes=10
        count = 0
        page_last_portal = []
        prev_search_last_portal = ""
        while count <= maxswipes:
            portal_list = self.driver.find_elements_by_id("portal_name")
            page_last_portal += portal_list[-1].text
            if prev_search_last_portal == portal_list[-1].text:
                raise FailedActionError("Portal %s not found." % _portalID)
            prev_search_last_portal = portal_list[-1]
            for elem in portal_list:
                if _portalID in elem.text:
                    target_portal = elem
                    target_portal.click()
                    return elem
            self.driver.swipe(start_x=360, start_y=900, end_x=360, end_y=300, duration=2000)
            if count == maxswipes:
                logger.info("Cannot find Portal %s after %s swipes" % (_portalID, maxswipes))
                raise FailedActionError("Portal ID: %s not found in list. Exiting." % _portalID)
            count += 1
            time.sleep(1)

    def pair_portal(self):
        pair_btn = self.find_element_by_id("button_1")
        pair_btn.click()

    def verify_pair_portal(self):
        show_customize_btn = self.wait_for_id('button_apply', 120)
        if (show_customize_btn == False):
            raise FailedActionError("onboard portal failed. Exiting.")

    def name_customize(self, _ssid):
        network_name = self.find_element_by_id("ssid_edit", 90)
        network_name.click()
        network_name.send_keys(_ssid)
        logger.info("Customize SSID as %s Done" %(_ssid))

    def password_customize(self, _password):
        network_pwd = self.find_element_by_id("password_edit",90)
        network_pwd.click()
        network_pwd.send_keys(_password)
        logger.info("Customize Password Done")

    def skip_customize(self):
        skip_btn = self.find_element_by_id("button_skip")
        skip_btn.click()
        logger.info("Skip Customize Setting")

    def click_apply_customize(self):
        apply_btn = self.find_element_by_id("button_apply")
        apply_btn.click()
        logger.info("Apply Customize Setting")

    def home_onboard(self):
        home_btn = self.find_element_by_id("button_home",90)
        home_btn.click()

    #Portal App Home page
    def internet(self):
        internet_btn = self.find_element_by_id("internet_button")
        internet_btn.click()

    def portal_setting(self):
        portal_btn = self.find_element_by_id("portal_button")
        portal_btn.click()

    def guest(self):
        guest_btn = self.find_element_by_id("guests_button")
        guest_btn.click()

    def device(self):
        device_btn = self.find_element_by_id("devices_button")
        device_btn.click()

    def add_portal(self):
        add_btn = self.find_element_by_id("add_new_portal")
        add_btn.click()

    def network_setting(self):
        setting_btn = self.find_element_by_id("network_settings")
        setting_btn.click()

    #def help(self):
    #def mynetwork(self):

    ### Internet Settings Page ###
    def show_wan_ip(self):
        wan_ip = self.find_element_by_id("wan_ip")
        logger.info("WAN IP Address = " + wan_ip.text)
        return wan_ip
    ### Portal Settings Page ###
    #def get_ssid(self):
    def change_ssid(self, _ssid):
        ssid_btn = self.find_element_by_id("ssid_2g_edit")
        ssid_btn.click()
        ssid_btn.clear()
        ssid_btn.send_keys(_ssid)
        logger.info("Customize SSID as %s Done" %(_ssid))

    #def get_password(self):
    def change_password(self, _password):
        pwd_btn = self.find_element_by_id("password_2g_edit")
        pwd_btn.click()
        pwd_btn.clear()
        pwd_btn.send_keys(_password)

    #def enable_wireless_security_mode(self):
    def show_advanced_device_settings(self):
        switch_btn = self.find_element_by_id("show_advance_switch")
        switch_btn.click()

    #def seperate_network(self):
    #def show_operating_channel(self):
    #def webgui_enable(self):
    def bridge_mode(self):
        bridge_mode_btn = self.find_element_by_id("bridge_mode_enabled_switch")
        bridge_mode_btn.click()

    def apply_setting(self):
        apply_btn = self.find_element_by_id("apply_settings")
        apply_btn.click()

    def wait_setting_change_done(self, timeout=120):
        back_btn = self.find_element_by_id("toolbar_back", timeout)
        back_btn.click()

    def check_change_ssid_success(self, ssid):
        if self.wait_for_id('ssid_2g_edit', 180) == True:
            ssid_view = self.find_element_by_id('ssid_2g_edit')
            if ssid_view.text == ssid:
                return True
            else:
                return False
        else:
            return False

    def check_change_password_success(self, password):
        if self.wait_for_id('password_2g_edit', 120) == True:
            show_password_button = self.find_element_by_id('show_password_2g_button')
            show_password_button.click()
            password_view = self.find_element_by_id('password_2g_edit')
            if password_view.text == password:
                return True
            else:
                return False
        else:
            return False
    #def compatibility_mode(self):
    ### Compatibility Mode ###
    #def compatibility_mode_enable(self):
    #def mode_a(self):
    #def mode_b(self):
    #def mode_c(self):
    #def apply_compatibility_mode(self):
    #def back_compatibility_mode(self):
    #Guest Page
    def back_user_and_guest(self):
        back_btn = self.find_element_by_id("toolbar_back", 120)
        back_btn.click()

    def add_guest_network(self):
        add_btn = self.find_element_by_id("add_new_guest_network")
        add_btn.click()

    def set_ssid_guest_network(self, ssid):
        network_name = self.find_element_by_id("ssid_edit", 90)
        network_name.click()
        network_name.send_keys(_ssid)
        logger.info("Customize SSID as %s Done" %(_ssid))

    def set_password_guest_network(self, password):
        network_pwd = self.find_element_by_id("password_edit",90)
        network_pwd.click()
        network_pwd.send_keys(_password)
        logger.info("Customize Password Done")

    def save_guest_network(self):
        save_btn = self.find_element_by_id("apply_settings")
        save_btn.click()
    def back_guest_network(self):
        back_btn = self.find_element_by_id("toolbar_back", 120)
        back_btn.click()
    ### Device Page ###
    def back_client_devices(self):
        back_btn = self.find_element_by_id("toolbar_back", 120)
        back_btn.click()
    #def show_client_devices(self):
    #def reflash_client_devices(self):
    #network setting Page
    def back_network_setting(self):
        back_btn = self.find_element_by_id("toolbar_back", 120)
        back_btn.click()
    #def check_update(self):
    #def accept_information(self):
    #def restart_network(self):
    #def accept_restart_network(self):
    #def cancel_restart_network(self):
    def remove_as_admin(self):
        remove_as_admin_btn = self.find_element_by_id("delete_network_button")
        remove_as_admin_btn.click()

    def accept_remove_as_admin(self):
        #apply_btn = self.driver.find_element_by_xpath('//android.widget.FrameLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[2]/android.widget.Button[2]')
        apply_btn = self.driver.find_element_by_xpath('//android.widget.Button[2]')
        apply_btn.click()

    def check_portal_onboard(self, timeout=10):
        return self.wait_for_id("portal_button", timeout)

    def check_in_main_page(self, timeout=10):
        return self.wait_for_id("buy_button", timeout)

    ### Tool ####
    def find_element_by_id(self, _id, timeout=10):
        """Custom find element by id with a 10 sec wait for item to show up"""
        element_id = _id
        if not ":id/" in _id:
            element_id = self.appPackageName + ':id/' + _id

        if self.wait_for_id(element_id, timeout) == True:
            return self.driver.find_element_by_id(element_id)

    def find_elements_by_id(self, _id, timeout=10):
        """Custom find element by id with a 10 sec wait for item to show up"""
        element_id = _id
        if not ":id/" in _id:
            element_id = self.appPackageName + ':id/' + _id

        if self.wait_for_id(element_id, timeout) == True:
            return self.driver.find_elements_by_id(element_id)

    def wait_for_id(self, _id, timeout=20):
        """Wait for id element to show up
        Has a default 10 sec timeout.
        """
        try:
            WebDriverWait(self.driver, timeout).until(
                    EC.presence_of_element_located((By.ID, _id))
                    )
            return True
        except TimeoutException:
            #print (self.driver.page_source)
            return False

    def find_element_by_xpath(self, _xpath, timeout=90):
        """Custom find element by xpath with a 10 sec wait for item to show up"""
        self.wait_for_xpath(_xpath, timeout)
        return self.driver.find_element_by_xpath(_xpath)

    def wait_for_xpath(self, _xpath, timeout=90):
        """Wait for id element to show up
        Has a default 10 sec timeout.
        """
        try:
            WebDriverWait(self.driver, timeout).until(
                    EC.presence_of_element_located((By.XPATH, _id))
                    )
            return True
        except TimeoutException:
            print (self.driver.page_source)
            return False

    def click_help(self):
        print "Click help button at the manual page"
        help_button = self.find_element_by_id('button_help')
        help_button.click()

    def click_quick_start(self):
        print "Quick start guide"
        helpList = self.driver.find_elements_by_xpath('//android.widget.ListView/android.widget.LinearLayout');
        helpList[0].click()

    def click_led_color(self):
        helpList = self.driver.find_elements_by_xpath('//android.widget.ListView/android.widget.LinearLayout');
        helpList[1].click()

    def click_help_center(self):
        helpList = self.driver.find_elements_by_xpath('//android.widget.ListView/android.widget.LinearLayout');
        helpList[2].click()

    def click_contact_us_button(self):
        contact_us = self.find_element_by_id('contact_us_button', 30)
        contact_us.click()

    def click_contact_us(self):
        helpList = self.driver.find_elements_by_xpath('//android.widget.ListView/android.widget.LinearLayout');
        helpList[3].click()

    def click_my_support_tickets(self):
        helpList = self.driver.find_elements_by_xpath('//android.widget.ListView/android.widget.LinearLayout');
        helpList[4].click()

    def click_rate_our_app(self):
        helpList = self.driver.find_elements_by_xpath('//android.widget.ListView/android.widget.LinearLayout');
        helpList[5].click()

    def click_quick_start_next(self):
        #Click quick start guide next button
        nextButton = self.find_element_by_id('button_next')
        nextButton.click()

    def click_skip(self):
        skipButton = self.find_element_by_id('option_button')
        skipButton.click()

    def click_cancel(self):
        #Click cancel button at the locating page.
        cancel_btn = self.find_element_by_id('button_1')
        cancel_btn.click()

    def click_home(self):
        #Click skip button at the customize page
        home_btn = self.find_element_by_id('button_home')
        home_btn.click()

    def click_customize_skip(self):
        #Click skip button at the customize page
        home_btn = self.find_element_by_id('button_skip', 60)
        home_btn.click()

    def check_pair_success(self):
        return self.wait_for_id('button_apply', 60)

    def check_customize_success(self):
        return self.wait_for_id('button_home', 120)

    def click_restart_network(self):
        #Click restart network button at the portal setting page.
        restart_network_btn = self.find_element_by_id("restart_network_button")
        restart_network_btn.click()

    def click_ok_button(self):
        #Click Remove as Admin button at the portal setting page.
        remove_admin_btn = self.find_element_by_id("android:id/button1")
        remove_admin_btn.click()

    def click_web_gui(self):
        switch = self.find_element_by_id("webgui_enabled_switch")
        switch.click()

    def click_separate_network(self):
        switch = self.find_element_by_id("separate_network_enabled_switch")
        switch.click()

    def click_beamforming(self):
        switch = self.find_element_by_id("beamforming_switch")
        switch.click()

    def click_compatibility_mode(self):
        view = self.find_element_by_id("compatibility_mode_view")
        view.click()

    def open_compatibility_mode(self):
        switch = self.find_element_by_id('compatibility_mode_enabled_switch')
        switch.click()

    def click_compatibility_mode_item(self, mode):
        el = self.find_elements_by_id('mode_item_layout', 60)
        el[mode].click()

    def click_home_add_mesh(self):
        mesh_btn = self.find_element_by_id('add_new_portal')
        mesh_btn.click()

    def click_add_mesh(self):
        add_mesh_btn = self.find_element_by_id('button_2')
        add_mesh_btn.click()

    def click_mesh_next(self):
        next_btn = self.find_element_by_id('button_next')
        next_btn.click()

    def find_portal_mesh(self, portalId):
        print('        Find mesh Portal %s', portalId)
        self.found_portal(portalId)

    def click_configure(self):
        #Click configure button at the locating page.
        try:
           self.find_element_by_id('button_1').click()
        except:
           return

    def mesh_all_done(self, timeout=10):
        mesh_all_done_btn = self.find_element_by_id('button_home', timeout)
        mesh_all_done_btn.click()

    def verify_mesh_success(self):
        mesh_success = self.wait_for_id('mesh_portal')
        return mesh_success

    def click_connection_type(self, timeout=10):
        connection_type_btn = self.find_element_by_id('internet_connection_type_button', timeout)
        connection_type_btn.click()

    def click_bridge_item(self):
        bridge_items = self.driver.find_elements_by_xpath('//android.widget.ListView/android.widget.LinearLayout');
        bridge_items[2].click()

    def click_bridge_switch(self, timeout=10):
        bridge_switch = self.find_element_by_id('bridge_mode_enabled_switch', timeout)
        bridge_switch.click()

    def apply_bridge_change(self):
        apply_btn = self.find_element_by_id('button_2')
        apply_btn.click()

    def click_pppoe_item(self):
        pppoe_items = self.driver.find_elements_by_xpath('//android.widget.ListView/android.widget.LinearLayout');
        pppoe_items[1].click()

    def click_static_ip_item(self):
        static_ip_items = self.driver.find_elements_by_xpath('//android.widget.ListView/android.widget.LinearLayout');
        static_ip_items[3].click()

    def set_account_pppoe(self, account):
        account_btn = self.find_element_by_id("login_edit", 90)
        account_btn.click()
        account_btn.send_keys(account)

    def set_password_pppoe(self, password):
        password_btn = self.find_element_by_id("password_edit", 90)
        password_btn.click()
        password_btn.send_keys(password)

    def set_service_pppoe(self, service):
        service_btn = self.find_element_by_id("service_name_edit", 90)
        service_btn.click()
        service_btn.send_keys(service)

    def set_ip_static_ip(self, ip):
        ip_edit = self.find_element_by_id("ip_edit", 90)
        ip_edit.click()
        ip_edit.send_keys(ip)

    def set_netmask_static_ip(self, netmask):
        netmask_edit = self.find_element_by_id("netmask_edit", 90)
        netmask_edit.click()
        netmask_edit.send_keys(netmask)

    def set_gateway_static_ip(self, gateway):
        gateway_edit = self.find_element_by_id("gateway_edit", 90)
        gateway_edit.click()
        gateway_edit.send_keys(gateway)

    def set_dns_static_ip(self, dns):
        dns_edit = self.find_element_by_id("dns_edit", 90)
        dns_edit.click()
        dns_edit.send_keys(dns)

    def click_turn_off_wireless_security(self):
        switch = self.find_element_by_id("security_mode_2g_switch")
        switch.click()

    def click_guest_button(self):
        guest = self.find_element_by_id('guests_button')
        guest.click()

    def click_add_guest_button(self):
        add_guest = self.find_element_by_id('add_new_guest_network')
        add_guest.click()

    def enter_guest_ssid(self, _ssid):
        ssid_btn = self.find_element_by_id("ssid_edit")
        ssid_btn.click()
        ssid_btn.clear()
        ssid_btn.send_keys(_ssid)

    def enter_guest_password(self, _password):
        password_btn = self.find_element_by_id("password_edit")
        password_btn.click()
        password_btn.clear()
        password_btn.send_keys(_password)

    def check_create_guest_network_success(self, ssid):
        if self.wait_for_id('guest_network_recyclerView', 120) == True:
            guest_list = self.driver.find_elements_by_id("user_name")
            for elem in guest_list:
                if ssid == elem.text:
                    return True
        return False

    def click_menu(self):
        menu = self.find_element_by_id('toolbar_menu')
        menu.click()

    def add_new_portal_button(self):
        add_btn = self.find_element_by_id('add_network_button')
        add_btn.click()

    def click_device_button(self):
        device = self.find_element_by_id('devices_button')
        device.click()

    def click_device_refresh_button(self):
        refresh = self.find_element_by_id('refresh')
        refresh.click()

    def swipe_guest_network(self):
        if self.wait_for_id('avatar_text_view', 60) == True:
            guestList = self.driver.find_elements_by_xpath('//android.support.v7.widget.RecyclerView/android.widget.FrameLayout');
            if len(guestList) > 0:
                # self.driver.swipe(100, 1400, 0, 1400, 2000)
                os.system("adb shell input swipe 100 1400 0 1400")

    def click_delete_guest(self):
        delete = self.find_element_by_id('delete_network_button')
        delete.click()

    def click_home_help_button(self):
        help_btn = self.find_element_by_id('network_help')
        help_btn.click()

    def click_edit_internet_settings(self):
        setting = self.find_element_by_id('edit_internet_settings_view')
        setting.click()

    def check_add_new_portal_success(self, ssid):
        self.click_menu()
        portal_list = self.driver.find_elements_by_id("network_name")
        for elem in portal_list:
            if ssid == elem.text:
                return True
        return False

if __name__ == '__main__':
    test = PAL_Android_Base()
    test.init_appium_server("5000", "2937", "19e0c824116e07e7")
    test.start_appium_server()
    test.stop_appium_server()
