import time

from PAL_Android_Function import PAL_Android_Function


class PAL_Android_TestCase(PAL_Android_Function):
    ROBOT_LIBRARY_SCOPE = 'TEST_SUITE'

    def setup_portal(self):
        self.click_setup_portal()
        self.verify_setup()

    def quick_start_guide(self):
        print('Help page')
        self.click_setup_portal()
        self.click_help()
        self.click_quick_start()
        self.click_quick_start_next()
        self.click_quick_start_next()
        self.click_quick_start_next()
        self.click_quick_start_next()
    	pass

    def finding_portal_skip(self, portalId):
        print('Finding portal (Press skip)')
        self.click_setup_portal()
        self.click_skip()
        self.click_for_allow()
        self.found_portal(portalId)

    def click_for_allow(self):
        print "Click allow button at the portal ask for device to allow to get location info"
        try:
            self.find_by_id('permission_allow_button', 'com.android.packageinstaller').click()
        except:
           return

    def cancel_find_portal(self):
        print('        Cancel Find Portal')
        self.click_setup_portal()
        self.click_skip()
        self.click_cancel()
        self.verify_cancel_find_portal()

    def onboard_portal(self, portalId):
        self.finding_portal_skip(portalId)
        self.pair_portal()
        if self.check_pair_success() == False:
            raise FailedActionError("Onboard portal failed. Exiting.")

    def skip_customize(self, portalId):
        self.onboard_portal(portalId)
        if self.check_pair_success() == True:
            self.skip_settings()
        else:
            raise FailedActionError("Onboard portal failed. Exiting.")

    def skip_settings(self):
        self.click_customize_skip()
        if self.check_customize_success() == True:
            self.click_home()
        else:
            raise FailedActionError("Customize failed. Exiting.")

    def apply_customize(self, portalId, ssid, password):
        self.onboard_portal(portalId)
        if self.check_pair_success() == True:
            self.customize_setting(ssid, password)
        else:
            raise FailedActionError("Onboard portal failed. Exiting.")

    def customize_setting(self, ssid, password):
        self.name_customize(ssid)
        self.password_customize(password)
        self.click_apply_customize()
        if self.check_customize_success() == True:
            self.click_home()
        else:
            raise FailedActionError("Customize failed. Exiting.")

    def launch_app_page(self,_phoneOS, _phoneOSVersion, _phoneUUID, _Port, _bootstarpPort):
    	self.start_appium_and_launch_app(_phoneOS, _phoneOSVersion, _phoneUUID, _Port, _bootstarpPort)

    def restart_network(self):
        if self.check_portal_onboard() == True:
            self.network_setting()
            self.click_restart_network()
            self.click_ok_button()
            self.click_ok_button()

    def enable_web_gui(self):
        print("        Enter Portal Device Enable Web GUI")
        if self.check_portal_onboard() == True:
            self.portal_setting()
            self.click_web_gui()
            self.apply_setting()
            self.wait_setting_change_done()

    def enable_separate_network(self):
        print("        Enter Portal Device Enable separate network")
        if self.check_portal_onboard() == True:
            self.portal_setting()
            self.click_separate_network()
            self.apply_setting()
            self.wait_setting_change_done()

    def enable_beamforming(self):
        print("        Enter Portal Device Enable Web GUI")
        if self.check_portal_onboard() == True:
            self.portal_setting()
            self.click_beamforming()
            self.apply_setting()
            self.wait_setting_change_done()

    def select_compatibility_mode(self, mode):
        print("select a mode: 0->A, 1->B, 2->C")
        if self.check_portal_onboard() == True:
            if mode > 2 or mode < 0:
                raise FailedActionError("Mode: %d not exist." %name)

            self.portal_setting()
            self.click_compatibility_mode()
            self.open_compatibility_mode()
            self.click_compatibility_mode_item(mode)
            self.apply_setting()
            self.wait_setting_change_done()

    def add_mesh(self, portalId):
        if self.check_portal_onboard() == True:
            self.click_home_add_mesh()
            self.click_add_mesh()
            self.click_mesh_next()
            self.click_mesh_next()
            self.find_portal_mesh(portalId)
            self.click_configure()
            self.mesh_all_done(300)
            if self.verify_mesh_success() == False:
                raise FailedActionError("Mesh failed: %s." %portalId)

    def set_bridge_onboard(self, portalId):
        self.finding_portal_skip(portalId)
        self.pair_portal()
        self.click_connection_type(90)
        self.click_bridge_item()
        self.click_bridge_switch(30)
        self.apply_bridge_change()
        if self.check_pair_success() == False:
            raise FailedActionError("Set bridge mode failed")

    def set_pppoe_onboard(self, portalId, account, password, service):
        self.finding_portal_skip(portalId)
        self.pair_portal()
        self.click_connection_type(90)
        self.click_pppoe_item()
        self.set_account_pppoe(account)
        self.set_password_pppoe(password)
        self.set_service_pppoe(service)
        self.apply_bridge_change()
        if self.check_pair_success() == False:
            raise FailedActionError("Set pppoe failed")

    def set_static_ip_onboard(self, portalId, ipAddress, netmask, gateway, dns):
        self.finding_portal_skip(portalId)
        self.pair_portal()
        self.click_connection_type(90)
        self.click_static_ip_item()
        self.set_ip_static_ip(ipAddress)
        self.set_netmask_static_ip(netmask)
        self.set_gateway_static_ip(gateway)
        self.set_dns_static_ip(dns)
        self.apply_bridge_change()
        if self.check_pair_success() == False:
            raise FailedActionError("Set static ip failed")
    	pass

    def change_router_ssid(self, ssid):
        if self.check_portal_onboard() == True:
            self.portal_setting()
            self.change_ssid(ssid)
            self.apply_setting()
            if self.check_change_ssid_success(ssid) == False:
                raise FailedActionError("Change ssid failed: %s" %ssid)
            else:
                print('change ssid success: %s' %ssid)

    def change_router_password(self, password):
        if self.check_portal_onboard() == True:
            self.portal_setting()
            self.change_password(password)
            self.apply_setting()
            if self.check_change_password_success(password) == False:
                raise FailedActionError("Change password failed: %s" %password)
            else:
                print('Change password success: %s' %password)

    def enable_turn_off_wireless_security(self):
        if self.check_portal_onboard() == True:
            self.portal_setting()
            self.click_turn_off_wireless_security()
            self.apply_setting()
            self.wait_setting_change_done()

    def create_guest_network(self, ssid, password):
        if self.check_portal_onboard() == True:
            self.click_guest_button()
            self.click_add_guest_button()
            self.enter_guest_ssid(ssid)
            self.enter_guest_password(password)
            self.apply_setting()
            if self.check_create_guest_network_success(ssid) == False:
                print('create guest network failed: %s' %ssid)
            else:
                print("create guest network success: %s" %ssid)

    def add_new_portal(self, portalId, ssid, password):
        if self.check_portal_onboard() == True:
            self.click_menu()
            self.add_new_portal_button()
            self.found_portal(portalId)
            self.pair_portal()
            if self.check_pair_success() == True:
                self.name_customize(ssid)
                self.password_customize(password)
                self.click_apply_customize()
                if self.check_customize_success() == True:
                    self.click_home()
                    if self.check_add_new_portal_success(ssid) == True:
                        print("add new portal success: %s" %ssid)
                    else:
                        print("add new portal failed: %s" %ssid)
                else:
                    print("customize portal failed: %s" %ssid)
            else:
                print("pair portal failed: %s" %ssid)

    def add_new_portal_skip_customize(self, portalId):
        if self.check_portal_onboard() == True:
            self.click_menu()
            self.add_new_portal_button()
            self.found_portal(portalId)
            self.pair_portal()
            if self.check_pair_success() == True:
                self.click_customize_skip()
                if self.check_customize_success() == True:
                    self.click_home()

    def refresh_devices(self):
        if self.check_portal_onboard() == True:
            self.click_device_button()
            self.click_device_refresh_button()

    def delete_guest_network(self):
        if self.check_portal_onboard() == True:
            self.click_guest_button()
            self.swipe_guest_network()
            self.click_delete_guest()

    def onboard_quick_start_guide(self):
        if self.check_portal_onboard() == True:
            self.click_home_help_button()
            self.click_quick_start()

    def onboard_led_color(self):
        if self.check_portal_onboard() == True:
            self.click_home_help_button()
            self.click_led_color()

    def onboard_help_center(self):
        if self.check_portal_onboard() == True:
            self.click_home_help_button()
            self.click_help_center()
            self.click_contact_us_button()

    def onboard_chart(self):
        if self.check_portal_onboard() == True:
            self.click_home_help_button()
            self.click_contact_us()

    def onboard_support_tickets(self):
        if self.check_portal_onboard() == True:
            self.click_home_help_button()
            self.click_my_support_tickets

    def onboard_rate(self):
        if self.check_portal_onboard() == True:
            self.click_home_help_button()
            self.click_rate_our_app()

    def set_bridge_in_ap_settings(self):
        if self.check_portal_onboard() == True:
            self.portal_setting()
            self.click_edit_internet_settings()
            self.click_bridge_switch(60)
            self.apply_setting()
            self.wait_setting_change_done(300)
