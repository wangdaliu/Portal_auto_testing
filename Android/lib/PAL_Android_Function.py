import time

from PAL_Android_Base import PAL_Android_Base


class PAL_Android_Function(PAL_Android_Base):
    ROBOT_LIBRARY_SCOPE = 'TEST_SUITE'
    def delete_network(self):
    	#close app
    	self.close_app()
    	#launch app
    	self.launch_app()
    	if self.check_portal_onboard() == True:
    		#Delete network
    		self.network_setting()
    		self.remove_as_admin()
    		self.accept_remove_as_admin()
    	self.check_in_main_page()

    def launch_app_goto_main_page(self,_phoneOS, _phoneOSVersion, _phoneUUID, _Port, _bootstarpPort):
    	self.start_appium_and_launch_app(_phoneOS, _phoneOSVersion, _phoneUUID, _Port, _bootstarpPort)
    	if self.check_portal_onboard() == True:
    		self.delete_network()
    		self.close_app()
    	else:
    		#close app
    		self.close_app()
    	#launch app
    	self.launch_app()
    	#check buy button or check setup button exist
    	print self.check_in_main_page()

    def find_portal(self, portalId):
        print('Find Portal %s', portalId)
        self.instruction_page()
        self.found_portal(portalId)
        pass

    def instruction_page(self):
        self.click_setup_portal()
        self.next_onboard()
        self.next_onboard()
        self.next_onboard()
        self.next_onboard()

    	#TODO
    	pass
